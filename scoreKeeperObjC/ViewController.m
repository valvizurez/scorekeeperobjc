//
//  ViewController.m
//  scoreKeeperObjC
//
//  Created by Victoria Alvizurez on 9/5/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@property (strong, nonatomic) IBOutlet UILabel *team1Lbl;
@property (strong, nonatomic) IBOutlet UILabel *team2Lbl;

@property (strong, nonatomic) IBOutlet UIStepper *team1Stepper;
@property (strong, nonatomic) IBOutlet UIStepper *team2Stepper;

@property (strong, nonatomic) IBOutlet UITextField *team1Text;
@property (strong, nonatomic) IBOutlet UITextField *team2Text;

@end

@implementation ViewController

- (IBAction)team1Stepper:(id)sender {
    NSUInteger value = self.team1Stepper.value;
    _team1Lbl.text = [NSString stringWithFormat:@"%0lu",value];
}

- (IBAction)team2Stepper:(id)sender {
    NSUInteger value = self.team2Stepper.value;
    _team2Lbl.text = [NSString stringWithFormat:@"%0lu",value];
}
- (IBAction)resetButton:(id)sender {
    _team1Lbl.text = @"0";
    _team2Lbl.text = @"0";
    _team1Stepper.value = 0;
    _team2Stepper.value = 0;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   // [_team1Text.self setDelegate:self];
  // self.team1Text.delegate = self;
    self.team1Text.delegate = self;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
