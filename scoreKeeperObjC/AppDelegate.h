//
//  AppDelegate.h
//  scoreKeeperObjC
//
//  Created by Victoria Alvizurez on 9/5/17.
//  Copyright © 2017 Victoria Alvizurez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end
